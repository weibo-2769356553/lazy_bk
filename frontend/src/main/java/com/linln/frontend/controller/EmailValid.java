package com.linln.frontend.controller;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author 小懒虫
 * @date 2020/01/01
 */
@Data
public class EmailValid implements Serializable {
    @NotEmpty(message = "邮箱不能为空")
    private String email;
    @NotEmpty(message = "验证码不能为空")
    private String emailCode;
}