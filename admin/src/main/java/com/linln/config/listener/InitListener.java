package com.linln.config.listener;

import cn.hutool.core.util.StrUtil;

import com.linln.common.constant.ParamConst;

import com.linln.common.enums.StatusEnum;

import com.linln.modules.system.domain.*;

import com.linln.modules.system.repository.ParamRepository;

import com.linln.modules.system.service.DictService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;


@Slf4j
@Component
@Order(1)

public class InitListener implements ApplicationListener<ApplicationReadyEvent>{
    @Autowired
    private  ParamRepository paramRepository;
    @Autowired
    private DictService dictService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {


//
//        setUpRootMenu();
//
//        long panelCnt = panelRepository.count();
//        if (panelCnt != NBPanel.PanelDom.values().length) {
//            log.info("「笔记博客」App 正在初始化首页右侧面板设置，请稍后...");
//            panelRepository.deleteAll();
//            setUpPanel();
//            log.info("「笔记博客」App 首页右侧面板初始化完毕...");
//        }
        Param nbParam = paramRepository.findFirstByName(ParamConst.INIT_STATUS);
        if (nbParam == null || StringUtils.isEmpty(nbParam.getValue()) ) {
         //   setUpAppInitialText();
            setDict();
            log.info("「笔记博客」App 参数初始化完毕！");
        } else {
            log.info("「笔记博客」App 已经完成初始化，略过初始化步骤。");
        }





    }

    /**
     * 设置首页的一些文本变量
     */
    private void setUpAppInitialText() {
        String[][] words = new String[][]{
           //     {ParamConst.INIT_STATUS, StrUtil.EMPTY, "初始化标识", "10"},
                {ParamConst.WEBSITE_TITLE, "小懒虫个人博客", "网站标题的文字", "10"},
                {ParamConst.FOOTER_WORDS, "", "页脚的文字", "10"},
                {ParamConst.INDEX_TOP_WORDS, "", "首页置顶文字", "10"},
                {ParamConst.WECHAT_PAY, "", "微信付款码", "11"},
                {ParamConst.ALIPAY, "", "支付宝付款码", "11"},
                {ParamConst.INFO_LABEL, "", "信息板内容", "10"},
                {ParamConst.WEBSITE_LOGO_WORDS, "", "网站logo的文字", "10"},
                {ParamConst.WEBSITE_LOGO_SMALL_WORDS, "", "网站logo的文字旁的小字", "10"},
                {ParamConst.COMMENT_NOTICE, "", "评论置顶公告", "10"},
                {ParamConst.PROJECT_TOP_NOTICE, "", "项目置顶公告", "10"},
                {ParamConst.MESSAGE_PANEL_WORDS, "", "留言板的提示信息文字", "10"},
                {ParamConst.MAIL_SMPT_SERVER_ADDR, StrUtil.EMPTY, "SMTP服务器", "8"},
                {ParamConst.MAIL_SMPT_SERVER_PORT, StrUtil.EMPTY, "SMTP端口号", "8"},
                {ParamConst.MAIL_SERVER_ACCOUNT, StrUtil.EMPTY, "发件人邮箱", "8"},
                {ParamConst.MAIL_SENDER_NAME, StrUtil.EMPTY, "发件人邮箱帐号（一般为@前面部分）", "8"},
                {ParamConst.MAIL_SERVER_PASSWORD, StrUtil.EMPTY, "邮箱登入密码", "8"},
                {ParamConst.APP_ID, StrUtil.EMPTY, "qq登录API的app_id", "9"},
                {ParamConst.APP_KEY, StrUtil.EMPTY, "qq登录API的app_key", "9"},
                {"system_name", "小懒虫", "后台系统名称", "9"},
                {"captcha_open", "true", "验证码是否开启", "9"}
        };
        saveParam(words);
    }

    /**
     * 保存一条参数记录
     *
     * @param params
     */
    private void saveParam(String[][] params) {
        Arrays.stream(params).forEach(
                hpw -> {
                    Param item = Param.builder()
                            .name(hpw[0])
                            .value(hpw[1])
                            .remark(hpw[2])
                            .status(StatusEnum.OK.getCode())
                            .build();
                    //保存每个设置的初始值
                    paramRepository.saveAndFlush(item);
                }
        );
    }
    /**
     * 设置字典的初始化值
     */
    private void setDict() {
        String[][] words = new String[][]{
                //     {ParamConst.INIT_STATUS, StrUtil.EMPTY, "初始化标识", "10"},
                {"数据状态", "DATA_STATUS", "2", "1:正常,2:冻结,3:删除"},
                {"字典类型", "DICT_TYPE", "2", "2:键值对"},
                {"用户性别", "USER_SEX", "2", "1:男,2:女"},
                {"菜单类型", "MENU_TYPE", "2", "1:目录,2:菜单,3:按钮"},
                {"搜索栏状态", "SEARCH_STATUS", "2", "1:正常,2:冻结"},
                {"日志类型", "LOG_TYPE", "2", "1:业务,2:登录,3:系统"},

        };
        Arrays.stream(words).forEach(
                hpw -> {
                    Dict item = Dict.builder()
                            .title(hpw[0])
                            .name(hpw[1])
                            .type(Byte.valueOf(hpw[2]))
                            .value(hpw[3])
                            .status(StatusEnum.OK.getCode())
                            .build();
                    //保存每个设置的初始值
                    dictService.save(item);
                }
        );
    }
}
