package com.linln.modules.system.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.http.HtmlUtil;
import com.linln.common.data.ArticleQueryBO;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.ToolUtil;
import com.linln.modules.system.domain.Article;
import com.linln.modules.system.domain.Cate;
import com.linln.modules.system.domain.NBTagRefer;
import com.linln.modules.system.domain.Tag;
import com.linln.modules.system.repository.ArticleRepository;
import com.linln.modules.system.repository.TagReferRepository;
import com.linln.modules.system.repository.TagRepository;
import com.linln.modules.system.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

import static cn.hutool.core.util.RandomUtil.randomInt;
import static java.util.stream.Collectors.toList;

/**
 * @author 小懒虫
 * @date 2020/01/01
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private TagReferRepository tagReferRepository;
    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public Article getById(Long id) {
        return articleRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<Article> getPageList(Example<Article> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return articleRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param article 实体对象
     */
    @Override
    @Transactional
    public Article save(Article article,String tagNames) {


        if (!StringUtils.isEmpty(article.getUrlSequence())) {
            boolean isExistUrl = articleRepository.countByUrlSequence(article.getUrlSequence()) > 0;
            if (isExistUrl) {
                throw new RuntimeException("已存在 url：" + article.getUrlSequence());
            }
        }
        setArticleSummaryAndTxt(article);
        decorateArticle(article);
        articleRepository.save(article);
        tagReferRepository.deleteByReferId(article.getId());
       // String[] tagNameArray = tagNames.split(",");
      //  saveTags(article,tagNameArray);
        return article;
    }




    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return articleRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }

    /**
     * 保存文章的 tags
     *
     * @param updateArticle
     * @param tagNameArray
     */
    private   void saveTags(Article updateArticle, String[] tagNameArray) {
        int cnt = 0;
        for (String name : tagNameArray) {
            Example<Tag> condition = Example.of(Tag.builder().name(name).build());
            boolean isExist = tagRepository.count(condition) > 0;
            long tagId = isExist ?
                    tagRepository.findByName(name).getId() :
                    tagRepository.save(Tag.builder().name(name).build()).getId();
            tagReferRepository.save(
                    NBTagRefer.builder()
                            .referId(updateArticle.getId())
                            .tagId(tagId)
                            .show(cnt <= 4)
                            .build()
            );
            cnt++;
        }
    }

    /**
     * 根据文章内容生成文章摘要
     *
     * @param article
     */
    private static void setArticleSummaryAndTxt(Article article) {
       // ParamService paramService = NBUtils.getBean(ParamService.class);
      //  int summaryLength = Integer.valueOf(paramService.getValueByName(NoteBlogV4.Param.ARTICLE_SUMMARY_WORDS_LENGTH));
        int summaryLength = 200;
        String clearContent = HtmlUtil.cleanHtmlTag(StrUtil.trim(article.getContent()));
        clearContent = StringUtils.trimAllWhitespace(clearContent);
        clearContent = clearContent.substring(0, clearContent.length() < summaryLength ? clearContent.length() : summaryLength);
        int allStandardLength = clearContent.length();
        int fullAngelLength = ToolUtil.fullAngelWords(clearContent);
        int finalLength = allStandardLength - fullAngelLength / 2;
        if (StringUtils.isEmpty(article.getSummary())) {
            article.setSummary(clearContent.substring(0, finalLength < summaryLength ? finalLength : summaryLength));
        }
       // article.setTextContent(clearContent);
    }

    /**
     * 装饰article的一些空值为默认值
     *
     * @param article
     */
    private static void decorateArticle(Article article) {
       // article.setPost(now());
        if (StringUtils.isEmpty(article.getViews())) {
            article.setViews(0);
        }
        if (StringUtils.isEmpty(article.getApproveCnt())) {
            article.setApproveCnt(0);
        }
        if (StringUtils.isEmpty(article.getAppreciable())) {
            article.setAppreciable(false);
        }
        if (StringUtils.isEmpty(article.getCommented())) {
            article.setCommented(false);
        }
        if (StringUtils.isEmpty(article.getTop())) {
            article.setTop(0);
        }
    }

    @Override
    public Page<Article> findBlogArticles(Pageable pageable, ArticleQueryBO articleQueryBO) {

        if (!StringUtils.isEmpty(articleQueryBO.getTime()))
            return   findBlogArticlesByTime(pageable,articleQueryBO);

        if (StringUtils.isEmpty(articleQueryBO.getTagSearch())) {
            String searchStr = articleQueryBO.getSearchStr() == null ? "" : articleQueryBO.getSearchStr();
            Article prob = Article.builder().content(searchStr)
                    .title(searchStr).build();
            if (articleQueryBO.getCateId() != null) {
                Cate cate = new Cate();
                cate.setId(articleQueryBO.getCateId());
                prob.setCate(cate);
            }
            prob.setStatus(StatusEnum.OK.getCode());

            ExampleMatcher matcher = ExampleMatcher.matching()
                    .withMatcher("title", ExampleMatcher.GenericPropertyMatcher::contains)
                    .withMatcher("content", ExampleMatcher.GenericPropertyMatcher::contains)
                    .withMatcher("status", ExampleMatcher.GenericPropertyMatcher::contains)
                    .withIgnorePaths("views", "approveCnt", "commented", "appreciable", "top","mdContent")
                    .withIgnoreNullValues();
            Example<Article> articleExample = Example.of(prob, matcher);
            //List<NBArticle> pages = articleRepository.findAll(articleExample);
            Page<Article> page = articleRepository.findAll(articleExample, pageable);
            List<Article> result = page.getContent();//.stream().filter(article -> !article.getStatus() == StatusEnum.OK.getCode()).collect(toList());
            return new PageImpl<>(result, pageable, page.getTotalElements());
        } else {
            String tag = ToolUtil.stripXSS(URLUtil.decode(articleQueryBO.getTagSearch(), "UTF-8"));
            long tagId = tagRepository.findByName(tag).getId();
            List<NBTagRefer> tagRefers = tagReferRepository.findByTagIdAndType(tagId, "article");
            List<Long> articleIds = tagRefers.stream().map(NBTagRefer::getReferId).distinct().collect(toList());
            if (CollectionUtils.isEmpty(articleIds)) {
                return Page.empty(pageable);
            } else {
                List<Article> articles = articleRepository.findByIdIn(articleIds, pageable.getPageNumber()*pageable.getPageSize(), pageable.getPageSize());
                return new PageImpl<>(articles, pageable, articleRepository.countByIdIn(articleIds));
            }
        }
    }

    public Page<Article> findBlogArticlesByTime(Pageable pageable, ArticleQueryBO articleQueryBO) {
        List<Article> articles = articleRepository.findByTime(articleQueryBO.getTime(), pageable.getPageNumber()*pageable.getPageSize(), pageable.getPageSize());
        return new PageImpl<>(articles, pageable, articleRepository.countByTime(articleQueryBO.getTime()));
    }
}